class LazyProperty(val initializer: () -> Int) {
    private var lazyValue: Int? = null
    private var initialized = false

    val lazy: Int
        get() {
            if (!initialized) {
                lazyValue = initializer()
                initialized = true
            }
            return lazyValue!!
        }
}